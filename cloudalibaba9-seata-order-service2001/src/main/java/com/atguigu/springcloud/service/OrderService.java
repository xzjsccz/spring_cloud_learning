package com.atguigu.springcloud.service;

import com.atguigu.springcloud.domain.Order;
import org.apache.ibatis.annotations.Param;

public interface OrderService {
    //1.新建订单
    void create(Order order);

}
