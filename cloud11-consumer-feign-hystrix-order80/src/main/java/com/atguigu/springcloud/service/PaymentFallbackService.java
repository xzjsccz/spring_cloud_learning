package com.atguigu.springcloud.service;

import org.springframework.stereotype.Component;

@Component
public class PaymentFallbackService implements PaymentHystrixService {
    @Override
    public String paymentInfo_ok(Integer id) {
        return "--paymentInfo_ok fall bcak ok";
    }

    @Override
    public String paymentInfo_error(Integer id) {
         return "--paymentInfo_error fall bcak error";
    }
}
