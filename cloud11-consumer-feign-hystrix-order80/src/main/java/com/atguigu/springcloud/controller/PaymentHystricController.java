package com.atguigu.springcloud.controller;

import com.atguigu.springcloud.service.PaymentHystrixService;
import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@Slf4j
//@DefaultProperties(defaultFallback = "payment_Global_FallbackMethod")
public class PaymentHystricController {
    @Resource
    private PaymentHystrixService paymentHystrixService;
    @GetMapping("/consumer/payment/hystrix/ok/{id}")
    public String paymentInfo_ok(@PathVariable("id")Integer id){
        String result=paymentHystrixService.paymentInfo_ok(id);
        return result;
    }
    //单个服务降级
//    @HystrixCommand(fallbackMethod = "paymentInfo_TimeOutHandler",commandProperties = {
//            @HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds",value = "1500")})
    @GetMapping("/consumer/payment/hystrix/error/{id}")
    public String paymentInfo_error(@PathVariable("id")Integer id){
        String result=paymentHystrixService.paymentInfo_error(id);
        return result;
    }

    public String paymentInfo_TimeOutHandler(@PathVariable("id")Integer id){
        return "出现错误,id"+id+"\t"+"超时了";
    }

    public String payment_Global_FallbackMethod(){
        return "Global全局异常，请稍后再试";
    }
}
